#include <RF24Network.h>
#include <RF24.h>
#include <SPI.h>

int node_id = 4;                    // Node ID 5

RF24 radio(7,8);                    // nRF24L01(+) radio attached using Getting Started board 
RF24Network network(radio);          // Network uses that radio

const uint16_t base_node = 00;       // Address of the other node in Octal format
const uint16_t this_node = 04;        // Address of our node in Octal format

const unsigned long interval = 30000; //ms  // How often to send 'hello world to the other unit

unsigned long last_sent;             // When did we last send?
unsigned long packets_sent;          // How many have we sent already

struct payload_t {                  // Structure of our payload
  int node_id;
  float x_val;
  float y_val;
  float z_val;
};

void setup(void)
{
  Serial.begin(9600);
  Serial.print("RF24Network Node ");
  Serial.println(node_id);
  
  SPI.begin();
  radio.begin();
  network.begin(90,this_node);
  radio.setDataRate(RF24_2MBPS);
}

void loop() {

//  testAccMeter();
  mainPrg();
  
}

void mainPrg(){
  network.update();                          // Check the network regularly 
  unsigned long now = millis();              // If it's time to send a message, send it!
  
  if ( now - last_sent >= interval  )
  {
    last_sent = now;
    
    int x_s = analogRead(A1);
    int y_s = analogRead(A2);
    int z_s = analogRead(A3);

    float x = (x_s * 5.0  / 1023.0);
    float y = (y_s * 5.0  / 1023.0);
    float z = (z_s * 5.0  / 1023.0);
    
    payload_t pl = {node_id, x, y, z};
    Serial.println("Sending payload...");
    
    String payload_ = "node: "+ (String)pl.node_id +" | x: "+(String)pl.x_val+" | y: "+(String)pl.y_val+" | z: "+ (String)pl.z_val;
    Serial.println(payload_);
    Serial.println();
    
    RF24NetworkHeader header(base_node);
    bool ok = network.write(header, &pl, sizeof(pl));    
    if (ok)
      Serial.println("ok.");
    else
      Serial.println("failed.");
  }  
}

void testAccMeter(){

    Serial.println("-------------------------------");
  
    int x_s = analogRead(A1);
    int y_s = analogRead(A2);
    int z_s = analogRead(A3);

    String output = "x: "+(String)x_s+" | y: "+(String)y_s+" | z: "+ (String)z_s;
    Serial.print("OUT ANALOG : ");
    Serial.println(output);

    float x = (x_s * 5.0  / 1023.0);
    float y = (y_s * 5.0  / 1023.0);
    float z = (z_s * 5.0  / 1023.0);

    output = "x: "+(String)x+" | y: "+(String)y+" | z: "+ (String)z;
    Serial.print("OUT ADC : ");
    Serial.println(output);

    payload_t pl = {node_id, x, y, z};       
    output = "node: "+ (String)pl.node_id +" | x: "+(String)pl.x_val+" | y: "+(String)pl.y_val+" | z: "+ (String)pl.z_val;
    Serial.print("OUT PAYLOAD : ");
    Serial.println(output);     
    Serial.println("-------------------------------");
    Serial.println();

    delay(5000);
}
