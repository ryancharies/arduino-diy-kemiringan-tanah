#include <Wire.h>
#include <ArduinoJson.h>
#include <RF24Network.h>
#include <RF24.h>
#include <SPI.h>
#include <RtcDS3231.h>

RtcDS3231<TwoWire> Rtc(Wire);

RF24 radio(7,8);                // nRF24L01(+) radio attached using Getting Started board 
RF24Network network(radio);      // Network uses that radio

const uint16_t this_node = 00;    // Address of our node in Octal format ( 04,031, etc)

struct payload_t {                  // Structure of our payload
  int node_id;
  float x_val;
  float y_val;
  float z_val;
};

DynamicJsonBuffer jsonBuffer;
JsonObject& json = jsonBuffer.createObject();

String timestamp = "";

void setup(void)
{
  Serial.begin(9600);
//  Serial.println("RF24Network Base Node");

  // --- SETUP RTC --- //
  Rtc.Begin();
  
  RtcDateTime compiled = RtcDateTime(__DATE__, __TIME__);
  printDateTime(compiled);
  Rtc.Enable32kHzPin(false);
  Rtc.SetSquareWavePin(DS3231SquareWavePin_ModeNone); 
  // --- END SETUP RTC --- //
  
  SPI.begin();
  radio.begin();
  network.begin(90,this_node);
  radio.setDataRate(RF24_2MBPS);
}

void loop(void){

  RtcDateTime now_ = Rtc.GetDateTime();
  printDateTime(now_);
  
  network.update();                  // Check the network regularly
  while ( network.available() ) {     // Is there anything ready for us?
    delay(50);
    
    RF24NetworkHeader header;        // If so, grab it and print it out
    payload_t pl;
    network.read(header, &pl, sizeof(pl)); 

    json["node"] = "node" + (String)pl.node_id;
    json["x"] = pl.x_val;
    json["y"] = pl.y_val;
    json["z"] = pl.z_val;
    json["tanggal"] = timestamp;

    json.printTo(Serial);
    Serial.print('\n');
    
  }
}

#define countof(a) (sizeof(a) / sizeof(a[0]))

void printDateTime(const RtcDateTime& dt)
{
    char datestring[20];

    snprintf_P(datestring, 
            countof(datestring),
            PSTR("%04u-%02u-%02u %02u:%02u:%02u"),
            dt.Year(),
            dt.Month(),            
            dt.Day(),                       
            dt.Hour(),
            dt.Minute(),
            dt.Second() );
            
    timestamp = datestring;
//    Serial.print(datestring);
}
