#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <FirebaseArduino.h>
#include <SoftwareSerial.h>

#define FIREBASE_HOST "diy-kemiringan-tanah-c5f0b.firebaseio.com"
#define FIREBASE_AUTH "eTYh1rkGaQhJnzn5d6elMIflRQ6vxkZpXUvOtn2m"
//#define WIFI_SSID "jangan marah-marah"
//#define WIFI_PASSWORD "hahahaha"

#define WIFI_SSID "Mitra Digital Teknologi"
#define WIFI_PASSWORD "@mdtsolut"

#define BAUD_RATE 9600
SoftwareSerial swSer(D3, D4);

int node_id;
float x_val;
float y_val;
float z_val;
String tanggal;

DynamicJsonBuffer jsonBuffer;
JsonObject& root = jsonBuffer.createObject();

void setup() {
  Serial.begin(BAUD_RATE);
  swSer.begin(BAUD_RATE);

  // connect to wifi.
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("connecting");
  while (WiFi.status() != WL_CONNECTED) {
   Serial.print(".");
   delay(500);
  }
  Serial.println();
  Serial.print("connected: ");
  Serial.println(WiFi.localIP());
  
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
  delay(2000);
}

void loop() {
    
  if(swSer.available() > 0) {
    String json = swSer.readStringUntil('\n');
    Serial.println(json);

    delay(50);
    
    JsonObject& root = jsonBuffer.parseObject(json);
    
    String areaAktif = Firebase.getString("areaAktif");
    Serial.println(areaAktif);

    String node = root["node"];
    String path = "area/" + areaAktif + "/" + node + "/logs";

    delay(50);
    String id = Firebase.push(path, root);
    if (Firebase.failed()) {
        Serial.print("pushing /logs failed:");
        Serial.println(Firebase.error());  
        return;
    }
    Serial.print("push logs id: ");
    Serial.println(id);
  }

}
